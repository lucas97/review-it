(ns app.application
  (:require
    [app.sample-servers.registry :refer [mock-http-server]]
    [com.fulcrologic.fulcro.application :as app]))

(defonce SPA (app/fulcro-app
              {:remotes {:remote (mock-http-server)}}))
