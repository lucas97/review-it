(ns app.ui
  (:require
    [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
    #?(:cljs [com.fulcrologic.fulcro.dom :as dom]
       :clj [com.fulcrologic.fulcro.dom-server :as dom])
    [com.fulcrologic.fulcro.application :as app]
    [com.fulcrologic.fulcro.algorithms.form-state :as fs]
    #?(:cljs [com.fulcrologic.fulcro.dom.events :as evt])
    [com.fulcrologic.fulcro.mutations :as m]
    [com.fulcrologic.fulcro.algorithms.tempid :as tempid]
    [app.apis.form]
    [app.application :refer [SPA]]))

(def content-form-status (fs/make-validator
                          (fn [form field]
                            (let [v (get form field "")]
                              (case field
                                (:content-form/name
                                 :content-form/quality
                                 :content-form/comment)
                                (boolean (seq v))
                                true)))))

(defsc ContentForm [this {:content-form/keys [id name quality comment] :as props}]
  {:query [:content-form/id :content-form/name :content-form/quality
           :content-form/comment fs/form-config-join]
   :form-fields #{:content-form/name
                  :content-form/quality
                  :content-form/comment}
   :ident :content-form/id}
  (dom/div {:data-form-status (str (content-form-status props))}
    (dom/p "Name: "
      (dom/input {:value (str name)
                  :data-status
                  (str (content-form-status props :content-form/name))
                  :onChange #?(:cljs (fn [evt]
                                       (m/set-value! this :content-form/name 
                                                     (evt/target-value evt)))
                               :clj (fn [evt] (pr-str evt)))}))
    (dom/button {:id "save"
                 :onClick #?(:clj (let [delta (fs/dirty-fields props true)]
                                    (pr-str [(app.apis.form/save {:delta delta})]))
                             :cljs (fn []
                                     (let [delta (fs/dirty-fields this true)]
                                       (comp/transact! this [(app.apis.form/save delta)]))))} "Add new content")))

(def ui-content-form (comp/factory ContentForm {:keyfn :content-form/id}))

(defsc Content [this {:content/keys [id name quality comment]}]
  {:query [:content/id :content/name :content/quality :content/comment]
   :ident :content/id
   :initial-state (fn [{:keys [id name quality comment]}]
                    {:content/id id
                     :content/name name
                     :content/quality quality
                     :content/comment comment})}
  (dom/div
   (dom/h4 name)
   (dom/p "Quality: " quality)
   (dom/p "Comment: " comment)))

(def ui-content (comp/factory Content {:keyfn :content/id}))

(defsc Review [this {:review/keys [id name contents]}]
  {:query [:review/id :review/name :review/contents]
   :ident :review/id
   :initial-state (fn [{:keys [id name] :as params}]
                    {:review/id id
                     :review/name name
                     :review/contents [(comp/get-initial-state Content {:id 1 :name "Dune" :quality 10 :comment "Pretty darn good!"})]})}
  (dom/div
   (dom/h3 name)
   (dom/ul
    (map ui-content contents))))

(def ui-review (comp/factory Review))

(defsc Root [this {:keys [books content-form]}]
  {:initial-state (fn [params] {:content-form (fs/add-form-config ContentForm [:content-form 2])
                                :books (comp/get-initial-state Review {:id 1 :name "Books"})})}
  (dom/div
   (dom/div
    (ui-content-form content-form))
   (dom/div
    (ui-review books))))

(comment
  (reset! (::app/state-atom SPA) {})  
  #(:cljs (app/current-state SPA))

  (app/schedule-render! SPA {:force-root? true})
  )
