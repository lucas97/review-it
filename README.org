* Ideas

** Review controller
   The main idea is to have a centralized database of every likes and
   dislikes. That could be

   - Books
   - Movies
   - Recipes


   and so on.

   The data structure could be something like:

   #+begin_src clojure
     {:reviews [:review/id 1}
      :review {1 {:review/id 1
		  :review/name "Book"
		  :review/contents [[:content/id 2]]}}
      :contents {2 {:content/id 2
		    :content/name "Dune"
		    :content/quality 10
		    :content/comment "Pretty darn good!"}}
      }
   #+end_src
